"""
This module consist of several methods, which rerurns lists of even numbers of odd numbers
Using RECURSION
"""
from random import randint as rndi

sample_list_integers = [x for x in range(rndi(1,2500), rndi(2500,3400))]

def isEvenOrOdd(num):
	"""Recursive Function is check number on even or not, returns true/false
	"""
	if num < 2:
		return num % 2 == 0
	else:
		return(isEvenOrOdd(num - 2))


if __name__ == '__main__':
	for x in sample_list_integers[1:999]:
		print(x, '->', isEvenOrOdd(x))
