"""
This module consist of several methods, which rerurns lists of even numbers of odd numbers
 Using FILTER 
"""
from random import randint as rndi

sample_list_integers = [x for x in range(rndi(1,2500), rndi(2500,3400))]


is_even = lambda x: not x % 2 
is_odd = lambda x: x % 2

for_even = list(filter(is_even, sample_list_integers))
for_odd = list(filter(is_odd, sample_list_integers))

if __name__ == '__main__':
	print('ODDS ->' ,for_odd)
	print('EVENS ->',for_even)