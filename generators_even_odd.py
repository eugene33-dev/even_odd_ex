"""
This module consist of several methods, which rerurns lists of even numbers of odd numbers
Using GENERATORS OF LISTS
"""
from random import randint as rndi

sample_list_integers = [x for x in range(rndi(1,2500), rndi(2500,3400))]

for_even = [x for x in sample_list_integers if not x % 2]
for_odd = [x for x in sample_list_integers if x % 2]

if __name__ == '__main__':
	print('ODDS ->' ,for_odd)
	print('EVENS ->',for_even)